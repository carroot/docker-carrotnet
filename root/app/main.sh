#!/usr/bin/with-contenv /bin/bash

set -eo pipefail
[[ $DEBUG == true ]] && set -x

export CARROT_BIRD_CONFIG_ROOT=/carrot/bird-config

if [ ! -e /carrot/install.lock ]; then
  echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
  echo "WARNING: You dont have carrot installed in this container,"
  echo "  or you fogot to mount /carrot into this container."
  echo "We will now sleep forever, you may use the following command"
  echo "  to install carrot in this container:"
  echo "$ docker exec -it <container_name> carrot-install"
  echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
  echo ""
  echo "If you are sure you don't need any install, please create a file"
  echo "  named /carrot/install.lock and we will skip this check."
  exit 0
fi

/sbin/iptennis

if [ -e /carrot/zerotier.enabled ]; then
  echo ">> Starting Zerotier-One ..."
  /bin/s6-svc -wu -T 5000 -u /var/run/s6/services/zerotier-one
  sleep 2
fi

if [ -e /carrot/wireguard.enabled ]; then
  echo ">> Starting WireGuard tunnels ..."
  /cbin/carrot-wg up
  sleep 2
fi

if [ -e /carrot/bgp.enabled ]; then
  echo ">> Starting bird ..."
  /bin/s6-svc -wu -T 5000 -u /var/run/s6/services/bird
  sleep 2
  echo ">> Updating ROA ..."
  /carrot/bird-config/updateroa.sh
  sleep 2
  echo ">> Starting cron ..."
  /bin/s6-svc -wu -T 5000 -u /var/run/s6/services/cron
  sleep 2
fi

echo ">>> Started carrot-docker."
