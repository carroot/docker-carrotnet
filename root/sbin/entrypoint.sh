#!/usr/bin/with-contenv /bin/bash

set -eo pipefail
[[ $DEBUG == true ]] && set -x

case ${1} in
  help)
    echo "Help yourself!"
    ;;
  init)
    exec /cbin/carrot-install
    ;;
  start)
    /app/main.sh
    /app/sleep.sh
    ;;
  *)
    exec "$@"
    ;;
esac

exit 0
