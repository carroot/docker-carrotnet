# Carrot Docker

## 安装

```bash
wget -O /usr/local/bin/carrotnet https://bitbucket.org/carroot/docker-carrotnet/raw/master/bin/carrotnet
chmod +x /usr/local/bin/carrotnet
```

## 初始化

```bash
# follow the on-screen instructions to setup
carrotnet init
carrotnet setup
```

## 使用

```bash
carrotnet restart # restart container
carrotnet exec bash # use bash inside container
eval $(carrotnet env) # get environment variables into current shell
```

## 卸载

```bash
eval $(carrotnet env)
carrotnet stop
docker rm -f "$CONTAINER_NAME"
docker network rm "$NETWORK_NAME"
# backup ssh keys or other important configs in $STORAGE_PATH
# and then rm -rf "$STORAGE_PATH"
```
