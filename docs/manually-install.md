# Carrot Docker

system tuning

```bash
sysctl -w net.ipv4.conf.all.forwarding=1
echo 'net.ipv4.conf.all.forwarding=1' >> /etc/sysctl.conf
sysctl -w net.ipv4.conf.all.rp_filter=0
echo 'net.ipv4.conf.all.rp_filter=0' >> /etc/sysctl.conf
sysctl -w net.ipv4.conf.all.accept_redirects=0
sysctl -w net.ipv4.conf.all.send_redirects=0
echo 'net.ipv4.conf.all.accept_redirects=0' >> /etc/sysctl.conf
echo 'net.ipv4.conf.all.send_redirects=0' >> /etc/sysctl.conf
```

docker setup

```bash
export IMAGE_NAME=carrot
export CONTAINER_NAME=carrot-edge
export STORAGE_PATH=/carrot
export NETWORK_NAME=carrot
export CARROT_SUBNET=33.33.2.56/29
export CARROT_EDGE_IP=33.33.2.58

mkdir -p $STORAGE_PATH

docker network create --subnet=$CARROT_SUBNET $NETWORK_NAME

docker run -d \
  -v $STORAGE_PATH:/carrot \
  -p 51800-51820:51800-51820/udp \
  -p 9993:9993/udp \
  --hostname $CONTAINER_NAME \
  --name $CONTAINER_NAME \
  --network $NETWORK_NAME \
  --ip $CARROT_EDGE_IP \
  -e CARROT_SUBNET=$CARROT_SUBNET \
  -e CARROT_EDGE_IP=$CARROT_EDGE_IP \
  --restart=always \
  --cap-add=NET_ADMIN --cap-add=SYS_ADMIN --device=/dev/net/tun \
  $IMAGE_NAME

docker exec -it $CONTAINER_NAME carrot-install

# 可以直接重启容器使安装生效
docker restart $CONTAINER_NAME
# 或者手动启动容器内的服务
docker exec $CONTAINER_NAME /app/main.sh
```
