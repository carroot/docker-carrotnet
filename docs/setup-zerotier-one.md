# 设置容器内的 zerotier-one

```bash
export CONTAINER_NAME=carrot-edge

docker exec $CONTAINER_NAME zerotier-cli info
docker exec $CONTAINER_NAME zerotier-cli join <networkname>
docker exec $CONTAINER_NAME zerotier-cli listnetworks
```
