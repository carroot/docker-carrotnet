# 设置 WireGuard 隧道

```bash
export CONTAINER_NAME=carrot-edge
export STORAGE_PATH=/carrot

# 复制范例配置
cp ./examples/wg0.conf $STORAGE_PATH/docker/etc/wireguard/wg0.conf

# 生成公私钥
./bin/genkey.sh

# 编辑范例配置
vim $STORAGE_PATH/docker/etc/wireguard/wg0.conf

docker exec $CONTAINER_NAME wg-quick up wg0
```
