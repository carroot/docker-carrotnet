# 常用脚本

```bash
export CONTAINER_NAME=carrot-edge

docker exec $CONTAINER_NAME ruby /carrot/bird-config/bgp-community.rb 10.147.20.42 50 encrypted
docker exec $CONTAINER_NAME birdc con
docker exec $CONTAINER_NAME birdc show route
```
